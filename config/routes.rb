Rails.application.routes.draw do
  resources :departments
  devise_for :companies
  # resources :homes
  root to: "departments#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
